## Containerized RLM
The project files contained here serve as a way to launch and host multiple RLM servers on a single physical or virtual machine.

The primary files include:

* Dockerfile
* docker-compose.yml
* restart-server.sh
* RLM files:
    * rlm
    * licenses
    * redgiant.set

The sections below will explain each of these in more detail.

### Dockerfile
The Dockerfile consists of the main build parameters for these RLM images.

Currently, the build follows these steps:

1. Pull the latest Ubuntu image
2. Update and install necessary packages
3. Copy the project files to the image's working directory, including:
    * redgiant.set binary file
    * Red Giant license files in their respective directories
    * "start.sh" script
4. Download the latest RLM files for Linux
5. Extract the RLM software files
6. Expose TCP ports 5053 (client comm. port), 5054 (web GUI port), and 5055 (ISV port)

### Docker Compose
This is the primary driver for the containerized RLM server.

Each license server has a section in the file, containing nearly identical instructions with one small difference.

The instructions follow these steps for each service:

1. Build the Docker image
2. Expose and map the necessary ports
3. Set the MAC address to 12:34:56:78:90:AB
4. Set the hostname to rlm-[org], where [org] is the organization's name
5. Set the entry point to "bash start.sh [org]", where [org] is the organization's name
6. Set up regular healthchecks that poll RLM's web interface, restarting the server if necessary

### Restart Server Script
This basic script uses the same [org] parameter in the Docker Compose file to build, create, and launch a single organization's server.

This can be used to launch a brand new server or rebuild and restart an existing server, for example: after updating an organization's licenses, restart the server to grant them access to the new licenses.

### RLM Files
There are a few basic RLM files that are necessary for each server: The redgiant.set binary ISV server and the organization's licenses.

These licenses are stored in the "licenses" directory in a subdirectory with the same [org] name.
