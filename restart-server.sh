#! /bin/bash

usage() {
    printf "Usage: restart-server.sh <server>\n"
    printf "\tserver:\tThe server/service name used by Docker\n"
    printf "\t\tto identify an image - based on the organization\n"
    printf "\tExample: ./restart-server.sh cbs\n"
}

SERVER=$1

if [ -z "$SERVER" ]; then
    echo Missing argument
    usage
    exit -1
else
    EXISTS=$(ls ./licenses | grep $SERVER)
    if [ -z $EXISTS ]; then
        echo Server $SERVER not found
    else
        echo $EXISTS exists, rebuilding and restarting...
        docker-compose build $SERVER && docker-compose up --no-start $SERVER && docker-compose start $SERVER
        echo Success!
    fi
fi