# Pull the latest alpine linux image
FROM ubuntu:latest

# Update the packages
RUN apt-get update && apt-get install -y wget iproute2 curl

# Set the new rlm directory as the working directory
WORKDIR /usr/bin/rlm

# Copy all the rlm files to the rlm directory
COPY ./rlm ./

# Expect an argument from docker-compose for this instance's organization name
ARG ORG

# Copy the appropriate license files for this organization to the rlm directory
COPY ./licenses/$ORG ./

# Download the newest version of RLM
RUN wget https://www.reprisesoftware.com/license_admin_kits/x64_l1.admin.tar.gz

# Extract the archive
RUN tar -xf /usr/bin/rlm/x64_l1.admin.tar.gz

# Expose the necessary ports
# EXPOSE 5053
# EXPOSE 5054
# EXPOSE 5055

# Start the RLM service
CMD ["bash", "/usr/bin/rlm/start.sh"]